## What I did as a Associate (Junior) Front End Engineer

## 2022.1.24

The first course that I've completed since working at Four Kitchens is this one https://drupalize.me/videos/twig-basics?p=1899 . My GitLab issues describes what I learned in detail! So far I'm noticing that most of the work that I will be doing is Twig + SASS. I'll also be doing some new React stuff as well! I'm glad that I have the foundation of HTML, CSS, and JavaScript! I believe that learning those languages + markup + styles, helps me learn new stuff so much more quickly! I'm definitely out of my depth, and need a lot of help. Though my team is really great at helping me learn! Its SO different learning on the job vs learning while I'm working a different job! All in all, I love learning on the job and hopefully will become more familiar with TWIG in these next few months! I also really want to learn more about React (which feels more natural to me than TWIG)

## 2021.12.15

I started with Four Kitchens on 11-30-2021. My Careerer has begun! I'm really happy to be in this field, and want to document all of the challenges + beauty that I find, thanks for reading. Here's to journal entry number one!
